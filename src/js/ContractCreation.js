function removeQuotes(str) {
    return str.replace(/['"]+/g, '');
}

function trigger(element) {
    element.addEventListener('keyup', (event) => {
        if(event.keyCode  === 13) {
            event.preventDefault();
            document.getElementById("create-button").click();
        } 
    });
}

function loading(isLoading) {
    const loader = document.querySelector('.loader');
    loader.innerHTML = "";
    if(isLoading) {
        loader.classList.add('active');
    } else {
        loader.classList.remove('active');
        loader.innerHTML = "Erstellen!";
    }
}

function setFeedback(msg, color, fsize) {
    if(msg == undefined) {
        msg = "";
    }
    if(color == undefined) {
        color = "red";
    }
    if(fsize == undefined) {
        fsize = "20px";
    }
    const feedback = document.getElementById("feedback");
    feedback.innerHTML = msg;
    feedback.style.color = color;
    if(fsize) {
        feedback.style.fontSize = fsize;
    } else {
        return;
    }
}

function addContract(form, place, time, title, money, music, description) {
    loading(true);

    //Convert time to timestamp

    $.ajax({
        beforeSend: function(xhrObj){
            //Get token from Cookie
            xhrObj.setRequestHeader("auth-token", removeQuotes(getCookie('authToken'))); 
        },
        type: 'POST',
        dataType: 'json',
            contentType: 'application/json',
            processData: false,
            data: JSON.stringify({
                "title": title,
                "place": place,
                "time": time,
                "form": form,
                "money": money,
                "music": music,
                "description": description,
            }), 
            url: "http://localhost:5000/api/createcontract",
            success: function(data) {
                setCookie("authToken", JSON.stringify(data.authToken));
                window.location.href = '../interface/availableContracts.html';
            },
            error: function(jqXhr) {
                if(jqXhr == undefined) {
                    setFeedback("Die Server sind momentan nicht erreichbar. Versuche Sie es später ernuet", "red", "20px");
                    loading(false);
                    return;
                }
                
                loading(false);
            }
    });
}

function showPage(number) {
    const pages = document.getElementsByClassName("page");
    
    pages[number].style.display = "flex";

    if (number == (pages.length - 2)) {
        document.querySelector('.loader').innerHTML = "Erstellen!";
      } else {
        document.querySelector('.loader').innerHTML = "Weiter";
      }
    if(currentPage == 1) {
        document.getElementsByClassName('form')[0].style.padding = "20px 0px";
        document.getElementById('create-button').style.margin = "0px";
        document.getElementsByClassName('register-title').style.margin = "0px";
    }
}

function changePage(number) {
    const pages = document.getElementsByClassName("page");

    //if(number == 1 && !validateContract()) return false;

    pages[currentPage].style.display = "none";

    currentPage = currentPage + number;
    setFeedback("Fast fertig! Nur noch ein paar Informationen.", "rgba(0, 0, 0, .5)");
    if(currentPage == 2) {
        document.getElementById('create-button').style.display = "none";
        document.getElementsByClassName('register-title')[0].style.display = "none";
        setFeedback("");
    } else {
        document.getElementById('create-button').style.display = "block";
        document.getElementsByClassName('register-title')[0].style.display = "block";
    }
    showPage(currentPage);
}

function toggleArrowIcon() {
    const icon = document.getElementById("arrow-icon");

    icon.classList.toggle("fa-sort-down");
    icon.classList.toggle("fa-sort-up");
}

function toggleDropdown() {
    const dropdownValues = document.querySelector('ul.dropdown-values');
    const icon = document.getElementById("arrow-icon");

    if(dropdownValues.style.display == "") {
        dropdownValues.style.display = "block";
        toggleArrowIcon();
        icon.style.transform = "translateY(-5px)";
    } else if(dropdownValues.style.display == "block") {
        dropdownValues.style.display = null;
        toggleArrowIcon();
        icon.style.transform = "translateY(-15px)";
    }
}

//Set current page
let currentPage = 0;
let readyToSend = false;
let contractValues = [];
showPage(currentPage);

//Event Listeners and Event Logic

const dropdownButton = document.getElementById("dropdown-btn");
const dropdownValues = document.querySelector('ul.dropdown-values');

dropdownButton.addEventListener('click', () => {
    toggleDropdown();
}); 

Array.prototype.map.call(dropdownValues.children, child => {
    child.addEventListener('click', () => {
        dropdownButton.innerHTML = dropdownButton.innerHTML.replace(dropdownButton.innerText, child.children[0].innerText); 
        toggleDropdown();
    });
});

const values = Object.values(document.querySelectorAll('div.input-container'));
values.forEach((value) => {
    if(value.children[1].nodeName != "TEXTAREA") {
        //Eventlistener on keyup(Enter)
        trigger(value.children[1]);
    }
    contractValues.push(value.children[1]);
});

document.getElementById('edit-button').addEventListener('click', () => {
    changePage(-1);
});

document.getElementById('continue-button').addEventListener('click', () => {
    location.href = "./availableContracts.html";
});

let modal = document.querySelector(".modal")
let closeBtn = document.querySelector(".modal-close-button");
contractValues[2].addEventListener('click', (e) => {
    e.preventDefault();
    modal.style.display = "flex";
});
closeBtn.onclick = function(){
  modal.style.display = "none";
}
window.onclick = function(e){
  if(e.target == modal){
    modal.style.display = "none";
  }
}

document.getElementById("modal-button").addEventListener('click', () => {
    const time = document.getElementById("time-input");
    const date = document.getElementById("date-input");

    if(time.value == null || time.value == "") {
        modalError(time, "Bitte geben Sie eine Uhrzeit an");
        return;
    }
    correct(time);

    if(date.value == null || date.value == "") {
        modalError(date, "Bitte geben Sie ein Datum an!");
        return;
    }
    correct(date);
    
    contractValues[2].value = time.value + "-" + date.value;

    contractValues[2].style.color = "black";
    modal.style.display = "none";
});

document.getElementById('create-button').addEventListener('click', () => {
    
    let type = contractValues[0];
    let place = contractValues[1];
    let time = contractValues[2];
    let title = contractValues[3];
    let payment = contractValues[4];
    let music = contractValues[5];
    let description = contractValues[6];
    console.log(contractValues[2].value);

    //Starting at Page 0
    if(currentPage == 0) { 
        //Get values from dropdown div because "children[0]" / select is the actual input
        let dropDownValue = contractValues[0].innerText;
        console.log(dropDownValue)
        if(dropDownValue == null || dropDownValue == "" || dropDownValue == "Wählen Sie eine Vertretung aus") {
            //children[1] is the styling element of the dropdown. So the border can be changed to red
            console.log(type)
            error(type, "Bitte wählen Sie eine Art aus!");
            return;
        } 
        //type equals dropDownButton
        correct(type);

        if(place.value == "null" || place.value == "") {
            error(place, "Bitte geben Sie einen Ort an!");
            return;
        }
        correct(place);

        if(time.value == "" || time.value == null || time.value == "Klicken...") {
            error(time, "Bitte geben Sie eine Uhrzeit an!");
            return;
        }
        correct(time);
        //Change Page 
        changePage(1);
    }
    if(currentPage == 1) {
        //Second Page

        if(title.value == "" || title.value == null) {
            error(title, "Bitte geben Sie einen Titel an!");
            return;
        }
        
        if(title.value.length < 6 || title.value.length > 40) {
            error(title, "Der Titel muss zwischen 6 und 40 Zeichen sein!");
            return;
        }
        correct(title);

        //Payment (optional?) 
        if(payment.value == "" || payment.value == null) {
            error(payment, "Bitte geben Sie eine Bezahlung an!");
            return;
        }

        if(payment.value.length > 3) {
            error(payment, "Die Bezahlung kann maximal 3-stellig sein!");
            return;
        }
        correct(payment);

        if(music.value == "" || music.value == null) {
            error(music, "Bitte geben Sie die benötigten Musikwerke an!");
            return;
        }
        correct(music);
        //EVENTUELL mehr musik validierung

        //Grant permission to send to API
        readyToSend = true;
    }

    if(readyToSend) {
        //addContract(type.value, place.value, time.value, title.value, payment.value, music.value, description.value);
        changePage(1);
    }
});