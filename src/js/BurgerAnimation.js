const slide = () => {

    const burger = document.querySelector('.burger');
    const navLinks = document.querySelector('.nav-links');
    const links = document.querySelectorAll('.nav-links li');
    const burgerBackground = document.querySelector('.burger-bg');


    burger.addEventListener('click', () => {

        if(navLinks.classList.contains("nav-active")) {
            navLinks.classList.remove('nav-active');
            setTimeout(() => {
               toggleMenu(navLinks, 'none');
            }, 1000)
            


         }
        else if (!navLinks.classList.contains("nav-active")) { 
           toggleMenu(navLinks, 'flex');
           setTimeout(() => {
               navLinks.classList.add('nav-active');
           }, 200);
         }

        const delay = 450;
        
        if(burgerBackground.classList.length == 1 && 
        burgerBackground.classList.contains("burger-bg")) {
               burgerBackground.classList.toggle("hide");
               setTimeout(() => {
                  burgerBackground.classList.remove("hide");
                  burger.classList.toggle("toggle");
               }, delay);
            }
        
        links.forEach((link, index) => {
          if(link.style.animation) {
             link.style.animation = '';
          } else {
            link.style.animation = `linkanim 0.5s ease forwards ${index / 7 + 0.5}s`;
          }
       });
   });
}

function toggleMenu(menu, displayProp) {
   menu.style.display = displayProp;
}  


function toggleLines() {
   const burger = document.querySelector('.burger');
   burger.classList.toggle("toggle");
   console.log("hi");
}

//Event listener
const burgerBackground = document.querySelector('.burger-bg');
burgerBackground.addEventListener('click', toggleLines);