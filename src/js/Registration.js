function trigger(element) {
    element.addEventListener('keyup', (event) => {
        if(event.keyCode  === 13) {
            event.preventDefault();
            document.getElementById("submit").click();
        } 
    });
}

function setFeedback(msg, color, fsize) {
    const feedback = document.getElementById('feedback');

    feedback.innerHTML = msg;
    feedback.style.color = color;
    if(fsize) {
        feedback.style.fontSize = fsize;
    } else {
        return;
    }
}

function loading(isLoading) {
    const loader = document.querySelector('.loader');
    loader.innerHTML = "";
    if(isLoading) {
        loader.classList.add('active');
    } else {
        loader.classList.remove('active');
        loader.innerHTML = "Registrieren";
    }
}

//Parsing Functions
function removeQuotes(str) {
    if(str = "") {
        return str
    }
    let newStr = str.replace(/['"]+/g, '');
    return newStr;
}

//START OF API CALL

async function register (lname, fname, email, password) {
    loading(true);

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        processData: false,
        data: JSON.stringify({
            "lastname": lname,
            "firstname": fname,
            "email": email,
            "password": password
        }), 
        url: "http://localhost:5000/api/register",
        success: function(data) {
        console.log(JSON.stringify(data));
            //Change checkCookies
            checkCookies(["lastname", "firstname", "email", "password"], [lname, fname, email, password]);
            window.location.href = '../interface/account.html';
        },
        error: function(jqXhr) {
            if(jqXhr.responseJSON == undefined) {
                setFeedback("Die Server sind momentan nicht erreichbar. Versuchen Sie es später erneut!", "red", "18px");
                loading(false);
                return;
            }
            setFeedback(removeQuotes(jqXhr.responseText), "red", "18px");
            loading(false);
        }
    });
}

//Button and ClickListener
const submit = document.getElementById("submit");
const email = document.getElementById("email-input");
const password = document.getElementById("password-input");
const passwordRepeat = document.getElementById("passwordrepeat-input");
const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

trigger(email);
trigger(password);
trigger(passwordRepeat);

submit.addEventListener('click', () => {
    //Validate
    //Email
    if(email.value == "" || email.value == null) {
        error(email, "Bitte geben Sie eine Email an!");
        return;
    }
    if(emailRegex.test(email.value)) {
        correct(email);
    } else {
        error(email, "Bitte geben Sie eine gültige Email an!");
        return;
    }

    //Passwords
    if(password.value == "" || password.value == null ) {
        error(password, "Bitte geben Sie ein Passwort an!");
        return;
    }
    if(password.value.length < 8 || password.value.length > 25) {
        error(password, "Das Passwort muss zwischen 8 und 25 Zeichen lang sein!");
        return;
    } else {
        correct(password);
    }

    if(passwordRepeat.value == "" || password.value == null) {
        error(passwordRepeat, "Bitte bestätigen Sie Ihr Passwort!");
        return;
    }
    if(passwordRepeat.value.length < 8 || passwordRepeat.value.length > 25) {
        error(passwordRepeat, "Das Passwort muss zwischen 8 und 25 Zeichen lang sein!");
        return;
    } else {
        correct(passwordRepeat);
    }
    console.log(password.value);
    console.log(passwordRepeat.value);

    if(password.value !== passwordRepeat.value) {
        error(password);
        error(passwordRepeat, "Die Passwörter stimmen nicht überein!");
        return;
    }
    

    register("ichbims", "vonFrontend", email.value, password.value);
});