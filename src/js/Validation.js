function error(elem, errText) {
    const feedback = document.getElementById("feedback");
    feedback.innerHTML = "";

    feedback.style.color = "red";
    feedback.style.fontSize = "18px";
    feedback.style.transition = "all 0.3s ease";

    elem.style.border = "4px solid red";

    if(!errText) { return; }
    feedback.innerHTML = errText.toString();    
}

function modalError(elem, errText) {
    const feedback = document.getElementById("modal-feedback");
    feedback.innerHTML = "";

    feedback.style.color = "red";
    feedback.style.fontSize = "18px";
    feedback.style.transition = "all 0.3s ease";

    elem.style.border = "4px solid red";

    if(!errText) { return; }
    feedback.innerHTML = errText.toString();   
}

function correct(elem) {
    elem.style.border = "4px solid lightgreen";
    setTimeout(() => {
        elem.style.border = "4px solid #f5c953";
    }, 2000);
}