function createNode(element) {
    return document.createElement(element);
}

function append(parent, element) {
    return parent.appendChild(element);
}

function capitalizeFirstLetter(string) {
    if(string === undefined) {
        return "Kein Titel vorhanden";
    }
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function parseDatetoReadable(time) {
   let date = new Date(time);
   let year = date.getFullYear();
   let month = date.getMonth() + 1;
   let dt = date.getDate();

   if (dt < 10) {
    dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }

    var weekday = new Array(7);
    weekday[0] = "SO";
    weekday[1] = "Mo";
    weekday[2] = "Di";
    weekday[3] = "Mi";
    weekday[4] = "Do";
    weekday[5] = "Fr";
    weekday[6] = "Sa";

    var day = weekday[date.getDay()];
    return(`${day} ${dt}.${month}.${year}`);
}

function calcCreationTime(creationTime) {

    let now = Date.now();   //Returns timestamp                                         
    let date = new Date(creationTime);  //Convert ISO Format to timestamp
    const millis = date.getTime();  //Saving timestamp into millis

    const timePassed = now - millis;
    let passedDate = new Date(timePassed);
    dateValues = [
        passedDate.getMonth() > 0 ? passedDate.getMonth() + 1 + " Monaten und " : '', 
        passedDate.getDate() > 0 ? passedDate.getDate() + " Tagen und " : '',
        passedDate.getHours() > 0 ? passedDate.getHours() + " Stunden " : '0 Stunden',
    ];
    return dateValues.join("");
}


function windowOnClick(event) {
    if (event.target === modal) {
        toggleModal();
    }
}

function addElement(element, styleClass, id, content) {
    if(element == null) { element = null; }
    if(styleClass == null) { styleClass = null; }
    if(id == null) { id = null }
    if(content == null) { content = null }
    let elem = createNode(element);
    elem.classList.add(styleClass);
    elem.id = id;
    elem.innerHTML = content;
    return elem;
}


function styleContract(data) {
    const container = document.getElementById('contract-div');
    let div = addElement("div", "contract");
    console.log(div);

    //Time Indicator
    let dot = addElement("div", "time-indicator", "", "");

    //SPAN for title
    let spanTitle = addElement("span", "contract-title", "", `${capitalizeFirstLetter(data.title)}`);

    //SPAN for creator- name and time
    let spanCreator = addElement("span", "creator", "", `von ${data.creatorEmail} vor ${calcCreationTime(data.creationTime)} erstellt`);

    //SPAN for location and time
    let spanLocation = addElement("span", "place", "", `In ${capitalizeFirstLetter(data.place)}`);
    let spanTime = addElement("span", "time", "", `${parseDatetoReadable(data.time)}`);

    //SPAN for Descripton title
    let spanFormTitle = addElement("span", "desc-title", "", "Description:");

    //SPAN for description
    let spanForm = addElement("span", "form", "", `${capitalizeFirstLetter(data.description)}`);

    //SPAN for more information
    let buttonInfo = addElement("button", "more-button", "", "Mehr");

    //Modal
    let modalDiv = addElement("div", "modal", "modal-div", "");
    let modalContent = addElement("div", "modal-content");
    let header = addElement("div", "heading");
    let closeButton = addElement("button", "close-button", "", "<i class='fas fa-times fa-2x'></i>");
    
    let wtitle = createNode("h1");
    wtitle.innerHTML = `${capitalizeFirstLetter(data.title)}`;
    wtitle.classList.add("modal-title");

    closeButton.addEventListener("click", () => {
        modalDiv.classList.toggle('show-modal');
    });
    window.addEventListener("click", (event) => {
        if (event.target === modalDiv) {
            modalDiv.classList.toggle('show-modal');
        }
    });
    buttonInfo.addEventListener('click', () => {
        modalDiv.classList.toggle('show-modal');
    });

    //Show elements
    append(container, div);
    append(div, dot);
    append(div, spanTitle);
    append(div, spanCreator);
    append(div, spanLocation);
    append(div, spanTime);
    append(div, spanFormTitle);
    append(div, spanForm);
    append(div, buttonInfo);

    //Show Modal Elements
    document.body.appendChild(modalDiv);  
    append(modalDiv, modalContent);
    append(modalContent, header);
    append(header, wtitle);
    append(header, closeButton);
    
}

function list(data) {
    return data.map((result) => {
        styleContract(result);
    });
}
 

//Fetch contracts from API
$.ajax({
    beforeSend: function(xhrObj){
        //Get token from Cookie
        xhrObj.setRequestHeader("auth-token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGRlYmRmMmUwOTA2ZDBhYzA1OTNmN2QiLCJpYXQiOjE1Nzc3MDgyMjF9.430i4Gcf5TptC0jwdk1C7cH-pbb80oL9uPX8brcMirg");
    },
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json',
    processData: false,
    url: "http://localhost:5000/api/contracts",
    success: function(data) {
        list(data);
    },
    error: function(jqXhr, textStatus, errorThrown) {
        const elem = document.getElementById('contract-div');
        let msg = createNode('p');
        msg.innerHTML = "Oops! Es ist ein Fehler aufgetreten. Versuchen Sie die Seite neu zu laden!";
        msg.classList.add("error-message");
        append(elem, msg);
        console.log(JSON.parse(errorThrown) + jqXhr.responseText);
    }
});