function trigger(element) {
    element.addEventListener('keyup', (event) => {
        if(event.keyCode  === 13) {
            event.preventDefault();
            document.getElementById("submit").click();
        } 
    });
}

function setFeedback(msg, color, fsize) {
    const feedback = document.getElementById("feedback");
    feedback.innerHTML = msg;
    feedback.style.color = color;
    if(fsize) {
        feedback.style.fontSize = fsize;
    } else {
        return;
    }
}

function loading(isLoading) {
    const loader = document.querySelector('.loader');
    loader.innerHTML = "";
    if(isLoading) {
        loader.classList.add('active');
    } else {
        loader.classList.remove('active');
        loader.innerHTML = "Registrieren";
    }
}

function login(email, password) {
    setFeedback("Melden Sie sich mit Ihrem bereits registrierten Konto an!", "black");
    loading(true);

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        processData: false,
        data: JSON.stringify({
            "email": email,
            "password": password
        }), 
        url: "http://localhost:5000/api/login",
        success: function(data) {
            setCookie("authToken", JSON.stringify(data.authToken));
            window.location.href = '/interface/availableContracts.html';
        },
        error: function(jqXhr) {
            if(jqXhr.responseText == undefined) {
                setFeedback("Die Server sind momentan nicht erreichbar. Versuchen Sie es später erneut!", "red", "20px");
                loading(false);
                return;
            }
            setFeedback(jqXhr.responseText, "red", "20px");
            loading(false);
        }
    });
}

const loginBtn = document.getElementById('submit');
const email = document.getElementById('email-input');
const password = document.getElementById('password-input');
const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;   

trigger(email);
trigger(password);  

loginBtn.addEventListener('click', () => {
    //Validate

    //Email
    if(email.value == "" || email.value == null) {
        error(email, "Bitte geben Sie Ihre bereits registrierte Email an!");
        return;
    }
    if(emailRegex.test(email.value)) {
        correct(email);
    } else {
        error(email, "Bitte geben Sie eine gültige Email an!");
        return;
    }

    if(password.value == "" || password.value == null ) {
        error(password, "Bitte geben Sie Ihr Passwort an!");
        return;
    } else {
        correct(password);
    }

    login(email.value, password.value);
});