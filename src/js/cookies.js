function setCookie(name, value) {
  let date = new Date();
  //Cookie will last 1 day max
  date.setTime(date.getTime() + (1*24*60*60*1000));
  let expires = "expires=" + date.toGMTString();
  //Validate Cookie Value
  let encodedValue = encodeURIComponent(value);
  //Update Path to real domain add secure flag
  document.cookie = name + "=" + encodedValue + ";" + expires + ";path=localhost:5500/; ";
}

function getCookieValue(name) {
  let cookieName = name + "=";
  let decodedCookie = decodeURIComponent(document.cookie);

  let values = decodedCookie.split(";");
  for(let i = 0; i < values.length; i++) {
    let value = values[i];
    while(value.charAt(0) == " ") {
      value = value.substring(1);
    }
    if(value.indexOf(name) == 0) {
      return value.substring(name.length, value.length);
    }
  }
  return "";
}

function deleteCookie(name) {
  //console.log(name + "= ;" + "expres");
  document.cookie = name + "=;" + " expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/;";
}

function checkCookie(name) {
  if(document.cookie.split(";").some((item) => item.trim().startsWith(name))) {
    return true;
  } else {
    return false;
  }
}

//Check content of cookie 
if(!checkCookie("authToken") && location.href.includes("/interface/")) {
  location.href = "/";
}